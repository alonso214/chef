#
# Cookbook Name:: nagios
# Recipe:: default
#
package 'install EPEL repo' do
  	package_name 'epel-release'
	action :install
end
package 'install nagios' do
  	package_name ['python-passlib','nagios','php']
	action :install
end

package 'install nagios-pluggins' do
  	package_name ['nagios-plugins-nrpe','nagios-plugins-ssh','nagios-plugins-disk','nagios-plugins-ping','nagios-plugins-load','nagios-plugins-procs','nagios-plugins-tcp','nagios-plugins-swap','nagios-plugins-http']
	action :install
end

package 'Install Apache' do
	case node[:platform]
	when 'redhat', 'centos'
		package_name 'httpd'
	when 'ubuntu', 'debian'
		package_name 'apache2'
	end
	action :install
end

service 'Stop nagios if is running' do
	service_name 'nagios'
	supports :status => false, :stop => true
	action [:stop]
end

#execute 'Allowing Nagios Linux user to login' do
user 'Allowing Nagios Linux user to login' do
	username 'nagios'
	shell '/bin/bash'
	home '/etc/nagios'
end

cookbook_file 'Copying authentication file' do
	path '/etc/nagios/passwd'
	source 'passwd'
end

template 'Copying cgi.cfg' do
	path	'/etc/nagios/cgi.cfg'
	source 'cgi.cfg'
end

template 'Copying nagios.cfg' do
	path	'/etc/nagios/nagios.cfg'
	source 'nagios.cfg'
end

service 'Starting Apache' do
	case node[:platform]
	when 'redhat', 'centos'
		service_name 'httpd'
	when 'ubuntu', 'debian'
		service_name 'apache2'
	end
	supports :restart => true
	action [:enable, :start]
end
service 'Starting nagios' do
	service_name 'nagios'
	supports :status => true, :restart => true, :start => true
	action [:enable, :start]
end
