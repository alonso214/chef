directory 'Removing default configuration' do
	path '/etc/nagios/objects'
	recursive true
	action :delete
end

remote_directory 'Copying objects folder' do
	path	'/etc/nagios/objects'
	owner 'nagios'
  	group 'nagios'
  	files_owner 'nagios'
  	files_group 'nagios'
	source 'objects'
	action :create
end

%w[ /etc/nagios /etc/nagios/objects ].each do |path|
 	directory path do
 		recursive true
  		owner 'nagios'
  		group 'nagios'
  		#mode ''
  	end
end

link '/tmp/file' do
	target_file '/usr/share/nagios/html/'
	to '/var/www/html'
end