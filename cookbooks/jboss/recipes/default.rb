#
# Cookbook Name:: jboss
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


# => Creating  user snap
user 'Creating snap Linux User' do
	username 'snap'
	home '/home/snap'
	shell '/bin/bash'
end

# => JBoss
remote_file 'Downloading Jboss' do
	path node['jboss']['tar_file']
	source node['jboss']['url']
	owner node['jboss']['user']
	group node['jboss']['group']
	mode '0755'
	action :create
end

bash 'Extracting Jboss' do
  	code "tar xzf #{node['jboss']['tar_file']} -C #{node['jboss']['extract_path']}"
  	user node['jboss']['user']
  	not_if { Dir.exists?("#{node['jboss']['home']}") }
end

# => Copying config files
template 'Copying standalone.conf' do
	path "#{node['jboss']['home']}bin/standalone.conf"
	source 'standalone.conf.erb'
	not_if { Dir.exists?("#{node['jboss']['home']}") }
end

template 'Copying standalone-ha.xml' do
	path "#{node['jboss']['home']}standalone/configuration/standalone-ha.xml"
	source 'standalone-ha.xml.erb'
	not_if { Dir.exists?("#{node['jboss']['home']}") }
end

directory "Creating JDBC path" do
  	path "#{node['jboss']['home']}modules/org/postgresql/main/"
  	recursive true
  	action :create
end

template 'Copying module.xml' do
	path "#{node['jboss']['home']}modules/org/postgresql/main/module.xml"
	source 'module.xml.erb'
	not_if { Dir.exists?("#{node['jboss']['home']}") }
end

remote_file 'Downloading JDBC module' do
	source "#{node['jdbc']['url']}"
	path "#{node['jboss']['home']}modules/org/postgresql/main/#{node['jdbc']['file']}"
	owner 'snap'
	group 'snap'
	mode '0664'
	action :create
end

# cookbook_file 'Copying sample.war file' do
# 	source "sample.war"
# 	path "#{node['jboss']['home']}standalone/deployments/sample.war"
#
# end

# => Firewall settings
execute 'Reload firewall-cmd' do
	command 'firewall-cmd --reload'
	action :nothing
end

bash 'Allowing 8080 port connections' do
  	code "firewall-cmd --zone=public --add-port=8080/tcp --permanent "
  	not_if %Q{ firewall-cmd --zone=public --list-all | grep ports | grep '8080/tcp' }
  	notifies :run, 'execute[Reload firewall-cmd]'
end

# bash 'Allowing http connections' do
#   	code "firewall-cmd --zone=public --add-service=http --permanent"
#   	not_if %Q{ firewall-cmd --zone=public --list-all | grep services | grep 'http ' }
# end

# bash 'Allowing https connections' do
#   	code "firewall-cmd --zone=public --add-service=https --permanent "
#   	not_if %Q{ firewall-cmd --zone=public --list-all | grep services | grep 'https ' }
# end

# => Adding jboss service
execute 'Reloading Systemd' do
	command 'systemctl daemon-reload'
	action :nothing
end

template 'Copying Jboss Service file' do
	path "/etc/systemd/system/jboss.service"
	source 'jboss.service.erb'
	notifies :run, 'execute[Reloading Systemd]', :delayed
end

service 'Start and enable jboss' do
	service_name 'jboss'
	supports :status => true, :restart => true, :start => true
	action [:enable, :start]
end
# log 'message' do
#   message "\nJDBC = #{node['jboss']['jdbc']}\nPOSTGRES Versio = #{node['postgres']['version']}"
#   level :info
# end
