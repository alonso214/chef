#
# Cookbook Name:: nrpe
# Recipe:: default
#
package 'install EPEL repo' do
        package_name 'epel-release'
        action :install
end

package 'install nrpe and pluggins' do
  	package_name ['nrpe','nagios-plugins-nrpe','nagios-plugins-ssh','nagios-plugins-disk','nagios-plugins-ping','nagios-plugins-load','nagios-plugins-procs','nagios-plugins-tcp','nagios-plugins-swap','nagios-plugins-http']
	action :install
end

service 'Starting nrpe' do
	service_name 'nrpe'
	supports :status => true, :restart => true, :start => true
	action [:enable, :start]
end