current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "admin"
client_key               "#{current_dir}/admin.pem"
validation_client_name   "manuel_org"
validation_key           "#{current_dir}/manuel_org.pem"
chef_server_url          "https://chef-server.local.net/organizations/manuel_org"
syntax_check_cache_path  "#{ENV['HOME']}/.chef/syntaxcache"
cookbook_path            ["#{current_dir}/../cookbooks"]
editor			"vi"
