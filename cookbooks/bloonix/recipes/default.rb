#
# Cookbook Name:: bloonix
#

package 'install EPEL repo' do
        package_name 'epel-release'
        action :install
end
package 'install bloonix' do
        package_name 'bloonix'
        action :install
end

service 'Start bloonix' do
	service_name 'bloonix'
        supports :status => true, :restart => true, :start => true
        action [:enable, :start]
end
