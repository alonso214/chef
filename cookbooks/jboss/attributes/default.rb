# => Jboss settings
default['jboss']['user'] = "snap"
default['jboss']['group'] = "snap"
default['jboss']['extract_path'] = "/home/#{node['jboss']['user']}/"
default['jboss']['file'] = "jboss-as-#{node['jboss']['version']}.Final.tar.gz"
default['jboss']['url'] = "http://download.jboss.org/jbossas/#{node['jboss']['version'][0..2]}/jboss-as-#{node['jboss']['version']}.Final/#{node['jboss']['file']}"
default['jboss']['home'] = "#{node['jboss']['extract_path']}jboss-as-#{node['jboss']['version']}.Final/"
default['jboss']['tar_file'] = "/tmp/#{node['jboss']['file']}" 

# => Calculate the memory heap and permsize based on the memory of the server
default['jboss']['memory'] = ("#{node['memory']['total']}".to_i * 0.8).to_i / 1024 
if node['jboss']['memory'] <= 2048
	default['jboss']['permsize'] = 128
elsif node['jboss']['memory'] > 2048 and default['jboss']['memory'] <= 4096
	default['jboss']['permsize'] = 256
elsif node['jboss']['memory'] > 4096 
	default['jboss']['permsize'] = 512
end

# => JDBC settings
if node['postgres']['version'] == "9.4"
	default['jdbc']['url'] = "https://jdbc.postgresql.org/download/postgresql-9.4.1208.jre7.jar"
	default['jdbc']['file'] = "po­stgresql-9­.4.1208.jr­e7.jar"
end